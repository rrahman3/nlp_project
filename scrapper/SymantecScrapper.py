import requests
from bs4 import BeautifulSoup
from csv import writer

# Please change the output file name
output_file_name = 'Mitre_Techniue_Description_1.csv'
input_file_name = ''

def get_all_attack_list(link, reports_link_list, class_name='table table-bordered table-light mt-2'):
    base_link = 'https://www.symantec.com'
    # reports_link_list = list()
    response = requests.get(link)
    print(link,end='\n\n')
    soup = BeautifulSoup(response.content, 'html.parser')
    # print(soup)
    table_details = soup.find_all('a', href=True)
    import re
    for link in table_details:
        if re.search(r'\d*-\d*-\d*-\d*', link['href']):
            link_ref = link['href']
            unique_ref = link_ref[link_ref.rfind('/') + 1 :]
            link_dict = dict()
            link_dict['title'] = link.text.replace('/','_') + '.' + unique_ref
            link_dict['data'] = base_link + link['href']
            # print(link_dict)
            reports_link_list.append(link_dict)
    return reports_link_list

def symantec_report_scrape(link, report_name):
    output_dict = dict()
    response = requests.get(link)
    # print(link, end='\n\n')
    soup = BeautifulSoup(response.content, 'html.parser')
    # print(soup.text)
    # print(soup.find_all(name_='panel-technicaldescription'))
    title = report_name #soup.find('h1').text
    report_data = ''
    contents = soup.find_all(class_='content')
    for content in contents:
        text = content.text
        if 'Updated:' in text:
            # print(text.strip())
            report_data += text.strip() + '\n'
        elif 'Technical Description' in text:
            # print(text.strip())
            report_data += text.strip() + '\n'

    output_dict['title'] = title
    output_dict['data'] = report_data
    return output_dict

def create_filename(location, title):
    import os
    path = os.path.join(os.getcwd(), location)
    if not os.path.isdir(path):
        try:
            print(os.getcwd())
            print()
            os.mkdir(path)
        except OSError:
            print("Creation of the directory %s failed" % path)
        else:
            print("Successfully created the directory %s " % path)

    file_name = os.path.join(path, title + '.txt')
    isFile = os.path.isfile(file_name)
    return file_name, isFile

def write_to_file(data_dict, file_name):
    with open(file_name,'w',encoding='UTF-8') as file:
        file.write(data_dict['data'])
    return

def scrape_symantec(link, title, location):
    """
    :param link: url of the symantec report
    :param title: title of the symantec report
    :param location: which location the reports will be saved in your machine.
            use only the folder name. it will automatically create the folder in your current directory.
    :return:
    """
    try:
        file_name, isFile = create_filename(location, title)
        if not isFile:
            # report_dict = symantec_report_scrape(link, title)
            # report_dict={'title':'ruhani','data':'ruhani'}
            # write_to_file(report_dict, file_name)
            print('Now Created: ',file_name)
        else:
            # print('Already Exist: ', file_name)
            return
    except:
        print("Error Occured")
    return

def cerate_link_manually():
    links = list()
    # links.append('https://www.symantec.com/security-center/a-z/A')
    base_link = 'https://www.symantec.com/security-center/a-z/'
    # sublinks = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    sublinks = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for char in sublinks:
        links.append(base_link + char)
    links.append(base_link + '_1234567890')
    # print(links)
    return links

def write_all_links_to_csv(links_list, file_name = r'report_links.csv'):
    import pandas
    links_data_frame = pandas.DataFrame(links_list)
    links_data_frame.to_csv(file_name, index=None, header=True)
    return

def scrape_all():
    links = cerate_link_manually()
    output_location = "symantec"
    total_length = 0
    for link in links:
        links_list = list()
        get_all_attack_list(link, links_list)
        # total_length += len(links_list)
        print(link,'\t',len(links_list))
        for link in links_list:
            scrape_symantec(link['data'], link['title'], output_location)
    # total_length = len(links_list)
    print(total_length)

def get_all_files(mypath):
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    return onlyfiles

def combine_all_reports(files):
    import os
    write_file = open('Symantec_reports_processed_technical_description_all.txt','w+',encoding='utf-8')
    for filename in files:
        processed_data = ''
        with open(os.path.join('symantec',filename),'r',encoding='utf-8') as temp_file:
            report_test = temp_file.read()
            processed_data += '-'*200 + '\n' + filename + '\n'

            technical_description_count = report_test.count('Technical Description')
            report_test = report_test.split('\n')
            for data in report_test:
                if 'Recommendations' in data:
                    break
                if 'SUBMITTING A SAMPLE TO SYMANTEC SECURITY RESPONSE' in data:
                    break
                if len(data) < 2:
                    continue
                if 'Technical Description' in data:
                    technical_description_count -= 1
                if technical_description_count < 1:
                    processed_data += data + '\n'
                    # write_file.write(data+'\n')

            processed_data += '-' * 200 + '\n'

            write_file.write(processed_data)
    write_file.close()
    return


def find_redundant_reports(files):
    import os
    redundant_file_list = list()
    write_file = open('Symantec_reports_processed_technical_description_!_is a heuristic detection.txt','w+',encoding='utf-8')
    for filename in files:
        if '!' in filename:
            processed_data = ''
            with open(os.path.join('symantec',filename),'r',encoding='utf-8') as temp_file:
                report_test = temp_file.read()
                if 'is a heuristic detection' in report_test:
                    print(filename)
                    processed_data += '-'*200 + '\n' + filename + '\n'
                    # write_file.write("-" * 200)
                    # write_file.write('\n')
                    # write_file.write(filename + '\n')

                    technical_description_count = report_test.count('Technical Description')
                    report_test = report_test.split('\n')
                    for data in report_test:
                        if 'Recommendations' in data:
                            break
                        if 'SUBMITTING A SAMPLE TO SYMANTEC SECURITY RESPONSE' in data:
                            break
                        if len(data) < 2:
                            continue
                        if 'Technical Description' in data:
                            technical_description_count -= 1
                        if technical_description_count < 1:
                            processed_data += data + '\n'
                            # write_file.write(data+'\n')

                    processed_data += '-' * 200 + '\n'
                    # write_file.write("-" * 200)
                    # write_file.write('\n')
                    if 'is a heuristic detection' in processed_data:
                        redundant_file_list.append(filename)
                        write_file.write(processed_data)
    write_file.close()
    return redundant_file_list

def word_analysis(files, directory, words):
    import os
    redundant_report_count = 0
    redundant_reports_list = list()
    for filename in files:
        with open(os.path.join(directory, filename), 'r', encoding='utf-8') as temp_file:
            report_test = temp_file.read()
            if words[0] in report_test or words[1] in report_test:
                # print(filename)
                redundant_report_count += 1
            else:
                # print(filename+'\n\n')
                # print(report_test)
                redundant_reports_list.append(filename)
    print(redundant_report_count)
    return redundant_report_count, redundant_reports_list

def only_technical_description(files, old, new):
    import os
    for filename in files:
        processed_data = ''
        with open(os.path.join(old,filename),'r',encoding='utf-8') as temp_file:
            report_test = temp_file.read()
            # processed_data += '-'*200 + '\n' + filename + '\n'

            technical_description_count = report_test.count('Technical Description')
            report_test = report_test.split('\n')
            for data in report_test:
                if 'Recommendations' in data:
                    break
                if 'SUBMITTING A SAMPLE TO SYMANTEC SECURITY RESPONSE' in data:
                    break
                if len(data) < 2:
                    continue
                if 'Technical Description' in data:
                    technical_description_count -= 1
                    continue
                if technical_description_count < 1:
                    processed_data += data + '\n'
                    # write_file.write(data+'\n')

            # processed_data += '-' * 200 + '\n'
        with open(os.path.join(new, filename), 'w', encoding='utf-8') as write_file:
            write_file.write(processed_data)
    return

def move_file(files, old_location, new_location):
    import os
    if not os.path.isdir(new_location):
        try:
            print(os.getcwd())
            print()
            os.mkdir(new_location)
        except OSError:
            print("Creation of the directory %s failed" % new_location)
        else:
            print("Successfully created the directory %s " % new_location)
    for file in files:
        os.rename(os.path.join(old_location,file), os.path.join(new_location,file))

def preprocess_reports():
    with open('symantec/W32.Beagle.AW@mm.2004-102910-4447-99.txt','r') as file:
        data = file.read()
        print(data.count('Technical Description'))


if __name__=='__main__':
    # scrape_all()
    ghaith_reports_directory = 'C:\\Users\\rrahman3\\Google Drive\\Study UNCC\\TTPDrill Takeover\\Raw Data\\SymantecThreatReports\\all-symantec-reports\\not-useful'
    ghaith_reports_directory_new = 'C:\\Users\\rrahman3\\Google Drive\\Study UNCC\\TTPDrill Takeover\\Raw Data\\SymantecThreatReports\\all-symantec-reports\\not-useful\\others'
    reports = 'only_description'
    # files = get_all_files(reports)
    # print(len(files))
    # redundant_files = find_redundant_reports(files)
    # move_file(redundant_files,'symantec','redundant')
    # combine_all_reports(files)
    # preprocess_reports()
    # only_technical_description(files,'symantec','only_description')
    # a, b = word_analysis(files, reports, ['is a detection','is a heuristic detection'])
    # move_file(b, reports, 'is_a_detection')
    import re
    text = 'Files that are detected as Trojan.Bamital.B!inf2 are considered  malicious. We suggest that any files you believe are incorrectly  detected be submitted to Symantec Security Response. For instructions on  how to do this using Scan and Deliver, read Submit Virus Samples'
    regex = r'^Files'
    print(re.search(text, regex))



